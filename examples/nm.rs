//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

// use std::env;

use netmaker::NetMaker;
// use pretty_assertions::assert_eq;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let nm = NetMaker::new("https://api.nm.dev.statehub.io", "friday", "friday");
    let _nodes = nm.get_all_nodes().await?;
    println!("{:?}", nm.token());

    let networks = nm.get_all_networks().await?;
    // let networks = dbg!(networks);

    if networks.iter().any(|network| network.netid == "ubernet") {
        println!("Removing ubernet (0)");
        nm.delete_network("ubernet").await?;
    }
    println!("Creating ubernet");
    nm.create_network("ubernet", "192.168.203.0/24").await?;
    // let ubernet = dbg!(ubernet);
    println!("Getting ubernet");
    let ubernet = nm.get_network("ubernet").await?;
    let ubernet1 = dbg!(ubernet);

    println!("Cycling key on all nodes of {}", ubernet1.netid);
    nm.cycle_public_keys_on_all_nodes(&ubernet1.netid).await?;
    // assert_eq!(ubernet1, ubernet2);

    println!("Creating Access Key for {}", ubernet1.netid);
    let key = nm.create_access_key(&ubernet1.netid, "uno", 1).await?;
    println!("{:?}", key);

    println!("Removing ubernet (1)");
    nm.delete_network(&ubernet1.netid).await?;

    Ok(())
}
