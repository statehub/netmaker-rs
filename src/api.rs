//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use reqwest::StatusCode;
use serde::{Deserialize, Serialize};
use thiserror::Error;

pub use auth::AuthToken;
pub use auth::SuccessfulUserLoginResponse;
pub use auth::UserAuthParams;
pub use network::AccessKey;
pub use network::CreateNetworkDto;
pub use network::Network;
pub use node::Node;

mod auth;
mod impls;
mod network;
mod node;

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
pub struct SuccessResponse<T> {
    #[serde(with = "http_serde::status_code", rename = "Code")]
    pub code: StatusCode,
    #[serde(rename = "Message")]
    pub message: String,
    #[serde(rename = "Response")]
    pub response: T,
}

#[derive(Clone, Debug, Error, Serialize, Deserialize)]
#[serde(rename_all = "PascalCase")]
#[error("[{code}] {message}")]
pub struct ErrorResponse {
    #[serde(with = "http_serde::status_code", rename = "Code")]
    pub code: StatusCode,
    #[serde(rename = "Message")]
    pub message: String,
}

mod yesno {
    use serde::{de, Deserialize, Deserializer, Serializer};

    pub(super) fn serialize<S>(value: &bool, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        if *value {
            serializer.serialize_str("yes")
        } else {
            serializer.serialize_str("no")
        }
    }

    pub(super) fn deserialize<'de, D>(deserializer: D) -> Result<bool, D::Error>
    where
        D: Deserializer<'de>,
    {
        match String::deserialize(deserializer)?.as_str() {
            "yes" => Ok(true),
            "no" => Ok(false),
            other => Err(de::Error::invalid_value(
                de::Unexpected::Str(other),
                &r#"Must be either "yes" or "no""#,
            )),
        }
    }
}
