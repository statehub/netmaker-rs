//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;
use thiserror::Error;

use crate::api;

#[derive(Debug, Error)]
#[error("NetMaker Fault")]
pub enum NetMakerFault {
    Unauthenticated(#[source] api::ErrorResponse),
    Other(#[source] api::ErrorResponse),
    InvalidResponse(#[from] json::Error),
}

impl From<api::ErrorResponse> for NetMakerFault {
    fn from(err: api::ErrorResponse) -> Self {
        Self::Other(err)
    }
}
