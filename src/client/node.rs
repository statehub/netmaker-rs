//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl NetMaker {
    /// Gets list of the registered nodes
    pub async fn get_all_nodes(&self) -> Result<Vec<api::Node>, error::NetMakerFault> {
        self.refresh_token().await?;
        let text = self.get("/api/nodes").await?;
        let nodes = json::from_str(&text)?;
        Ok(nodes)
    }
}
