//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl NetMaker {
    /// Gets list of all the registered networks
    pub async fn get_all_networks(&self) -> Result<Vec<api::Network>, error::NetMakerFault> {
        self.refresh_token().await?;
        let text = self.get("/api/networks").await?;
        let networks = json::from_str(&text)?;
        Ok(networks)
    }

    /// Gets info on a specified network
    pub async fn get_network(&self, name: &str) -> Result<api::Network, error::NetMakerFault> {
        self.refresh_token().await?;
        let path = format!("/api/networks/{}", name);
        let text = self.get(path).await?;
        let network = json::from_str(&text)?;
        Ok(network)
    }

    /// Creates a new network with given name and IPv4 address range
    pub async fn create_network(
        &self,
        name: &str,
        range: &str,
    ) -> Result<(), error::NetMakerFault> {
        self.refresh_token().await?;
        let dto = api::CreateNetworkDto {
            address_range: range.to_string(),
            netid: name.to_string(),
        };
        let text = self.post("/api/networks", dto).await?;
        debug_assert!(text.is_empty());
        Ok(())
    }

    /// Deletes specified network
    pub async fn delete_network(&self, name: &str) -> Result<(), error::NetMakerFault> {
        self.refresh_token().await?;
        let path = format!("/api/networks/{}", name);
        let success = self.delete(path).await?;
        let success = json::from_str::<json::Value>(&success).expect("Delete invalid JSON");
        debug_assert_eq!(success, json::json!("success"));
        Ok(())
    }

    /// Initiates public keys refresh on all the registered nodes
    pub async fn cycle_public_keys_on_all_nodes(
        &self,
        netid: &str,
    ) -> Result<(), error::NetMakerFault> {
        self.refresh_token().await?;
        let path = format!("/api/networks/{}/keyupdate", netid);
        let _text = self.post(path, ()).await?;
        // cycle_public_keys call returns broken (all fields are empty) api::Network object,
        // so we ignore JSON conversion errors here.
        // let network = json::from_str(&text)?;
        Ok(())
    }

    /// Creates new access key required for registering new node.
    /// The max number of uses for this key will be limited to `uses`.
    pub async fn create_access_key(
        &self,
        network: &str,
        name: &str,
        uses: i32,
    ) -> Result<api::AccessKey, error::NetMakerFault> {
        self.refresh_token().await?;
        let path = format!("/api/networks/{}/keys", network);
        let key = api::AccessKey::new(name, uses);
        let text = self.post(path, &key).await?;
        let key = json::from_str(&text)?;
        Ok(key)
    }
}
