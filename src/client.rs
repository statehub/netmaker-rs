//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::cell::{Ref, RefCell};
use std::fmt;
use std::time;

use serde::{de, ser};
use serde_json as json;

use crate::api;
use crate::error::{self, NetMakerFault};

mod network;
mod node;

/// NetMaker client object. Controls all the interaction with NetMaker server
#[derive(Clone, Debug)]
pub struct NetMaker {
    base: String,
    username: String,
    password: String,
    token: RefCell<Option<String>>,
    user_agent: String,
    connect_timeout: time::Duration,
}

impl NetMaker {
    /// Creates new client object with specified `url`. Uses `username` and `password`
    /// authentication issuing authentication token. Expiring token is refreshed automatically
    /// as needed.
    pub fn new(url: &str, username: &str, password: &str) -> Self {
        let base = url.to_string();
        let username = username.to_string();
        let password = password.to_string();
        let token = RefCell::new(None);
        let user_agent = format!("{}/{}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));
        let connect_timeout = time::Duration::from_secs(5);

        Self {
            base,
            username,
            password,
            token,
            user_agent,
            connect_timeout,
        }
    }

    async fn refresh_token(&self) -> Result<(), error::NetMakerFault> {
        if self.token_expired() {
            let token = self
                .authenticate(&self.username, &self.password)
                .await
                .map_err(NetMakerFault::Unauthenticated)?
                .authtoken;
            self.token.replace(Some(token));
        }
        Ok(())
    }

    fn token_expired(&self) -> bool {
        if let Some(token) = self.token().as_deref() {
            let token: Result<jwt::Token<jwt::Header, jwt::Claims, _>, _> =
                jwt::Token::parse_unverified(token);
            if let Ok(token) = token {
                if let Some(exp) = token.claims().registered.expiration {
                    if let Ok(duration) = time::SystemTime::now().duration_since(time::UNIX_EPOCH) {
                        return duration.as_secs() > exp - 30;
                    }
                }
            }
        }
        true
    }

    pub fn token(&self) -> Ref<'_, Option<String>> {
        self.token.borrow()
        // Ref::map(token, Option::as_ref)
    }

    async fn authenticate(
        &self,
        username: &str,
        password: &str,
    ) -> Result<api::SuccessfulUserLoginResponse, api::ErrorResponse> {
        let username = username.to_string();
        let password = password.to_string();
        let body = api::UserAuthParams { username, password };
        let text = self.post("/api/users/adm/authenticate", body).await?;
        let response =
            json::from_str::<api::SuccessResponse<api::SuccessfulUserLoginResponse>>(&text)?;
        Ok(response.response)
    }

    // fn inspect<T>(&self, output: &Output<T>)
    // where
    //     T: fmt::Debug,
    // {
    //     log::debug!("Output {:?}", output);
    // }

    pub fn url(&self, path: impl fmt::Display) -> String {
        format!("{}{}", self.base, path)
    }

    async fn delete<P>(&self, path: P) -> Result<String, api::ErrorResponse>
    where
        P: fmt::Display,
    {
        let url = self.url(path);
        self.client()
            .await?
            .delete(url)
            .optionally_bearer_auth(self.token().as_deref())
            .inspect()
            .send()
            // .retry()
            .await?
            .error_for_status2()
            .await
        // .inspect(|output| self.inspect(output))
    }

    async fn get<P>(&self, path: P) -> Result<String, api::ErrorResponse>
    where
        P: fmt::Display,
    {
        let url = self.url(path);
        self.client()
            .await?
            .get(url)
            .optionally_bearer_auth(self.token().as_deref())
            .inspect()
            .send()
            // .retry()
            .await?
            .error_for_status2()
            .await
        // .inspect(|output| self.inspect(output))
    }

    async fn _head<P>(&self, path: P) -> Result<(), api::ErrorResponse>
    where
        P: fmt::Display,
    {
        let url = self.url(path);
        self.client()
            .await?
            .head(url)
            .optionally_bearer_auth(self.token().as_deref())
            .inspect()
            .send()
            // .retry()
            .await?
            .error_for_status2()
            .await
            .map(|_| ())
        // .inspect(|output| self.inspect(output))
    }

    async fn post<P, B, T>(&self, path: P, body: B) -> Result<String, api::ErrorResponse>
    where
        P: fmt::Display,
        B: Into<Option<T>>,
        T: ser::Serialize,
    {
        let body = body.into();
        let url = self.url(path);
        self.client()
            .await?
            .post(url)
            .optionally_bearer_auth(self.token().as_deref())
            .inspect()
            .optionally_json(body.as_ref())
            .send()
            .await?
            .error_for_status2()
            .await
        // .inspect(|output| self.inspect(output))
    }

    async fn _put<P, T>(&self, path: P) -> Result<T, api::ErrorResponse>
    where
        P: fmt::Display,
        T: de::DeserializeOwned + ser::Serialize + fmt::Debug,
    {
        let url = self.url(path);
        self.client()
            .await?
            .put(url)
            .optionally_bearer_auth(self.token().as_deref())
            .inspect()
            .send()
            // .retry()
            .await?
            .error_for_status3()
            .await
        // .inspect(|output| self.inspect(output))
    }

    async fn client(&self) -> reqwest::Result<reqwest::Client> {
        reqwest::Client::builder()
            .user_agent(&self.user_agent)
            .connect_timeout(self.connect_timeout)
            .build()
    }
}

#[async_trait::async_trait]
trait ResponseExt: Sized {
    async fn status_and_bytes(self) -> reqwest::Result<(reqwest::StatusCode, bytes::Bytes)>;

    async fn status_and_text(self) -> reqwest::Result<(reqwest::StatusCode, String)>;

    async fn error_for_status2(self) -> Result<String, api::ErrorResponse> {
        let (status, text) = self.status_and_text().await?;

        if status.is_success() {
            Ok(text)
        } else {
            Err(api::ErrorResponse::from_split_response(status, text))
        }
    }

    async fn error_for_status3<T>(self) -> Result<T, api::ErrorResponse>
    where
        T: de::DeserializeOwned,
    {
        let (code, text) = self.status_and_text().await?;

        println!("ES3:\n{}", text);
        if let Ok(success) = json::from_str(&text) {
            return Ok(success);
        }

        if let Ok(error) = json::from_str::<api::ErrorResponse>(&text) {
            return Err(error);
        }

        Err(api::ErrorResponse::from_split_response(code, text))
    }
}

#[async_trait::async_trait]
impl ResponseExt for reqwest::Response {
    async fn status_and_bytes(self) -> reqwest::Result<(reqwest::StatusCode, bytes::Bytes)> {
        let status = self.status();
        let bytes = self.bytes().await?;
        Ok((status, bytes))
    }

    async fn status_and_text(self) -> reqwest::Result<(reqwest::StatusCode, String)> {
        let status = self.status();
        let text = self.text().await?;
        Ok((status, text))
    }
}

trait Inspector {
    fn inspect(self) -> Self;
}

impl Inspector for reqwest::RequestBuilder {
    fn inspect(self) -> Self {
        if let Some(request) = self.try_clone().and_then(|builder| builder.build().ok()) {
            log::trace!("{} {}", request.method(), request.url());

            request.headers().iter().for_each(|(header, value)| {
                if header == reqwest::header::AUTHORIZATION {
                    log::trace!("{}: {}", header, "[REDACTED]");
                } else {
                    log::trace!("{}: {}", header, String::from_utf8_lossy(value.as_bytes()))
                }
            });
        }

        self
    }
}

trait Optionally {
    fn optionally_bearer_auth(self, token: Option<&str>) -> Self;
    fn optionally_json<T>(self, body: Option<&T>) -> Self
    where
        T: ser::Serialize;

    fn optionally<T, F>(self, option: Option<T>, f: F) -> Self
    where
        F: FnOnce(Self, T) -> Self,
        Self: Sized,
    {
        if let Some(option) = option {
            f(self, option)
        } else {
            self
        }
    }
}

impl Optionally for reqwest::RequestBuilder {
    fn optionally_bearer_auth(self, token: Option<&str>) -> Self {
        self.optionally(token, Self::bearer_auth)
    }

    fn optionally_json<T>(self, body: Option<&T>) -> Self
    where
        T: ser::Serialize,
    {
        self.optionally(body, Self::json)
    }
}
