//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use super::*;

const NETWORKS: &str = r#"[
        {"addressrange":"10.200.0.0/16","addressrange6":"fd39:75a7:808f:649d::/64","displayname":"datapath","netid":"datapath","nodeslastmodified":1634732506,"networklastmodified":1634647969,"defaultinterface":"nm-datapath","defaultlistenport":51821,"nodelimit":999999999,"defaultpostup":"","defaultpostdown":"","keyupdatetimestamp":1634647969,"defaultkeepalive":20,"defaultsaveconfig":"no","accesskeys":[{"name":"nodes200","value":"cyVXDZ5n53IAkpuP","accessstring":"eyJjb3JlZG5zYWRkciI6IjU0LjI0MS42MC4yNDEiLCJhcGljb25uIjoiYXBpLm5tLmRldi5zdGF0ZWh1Yi5pbzo0NDMiLCJhcGlob3N0IjoiYXBpLm5tLmRldi5zdGF0ZWh1Yi5pbyIsImFwaXBvcnQiOiI4MDgxIiwiZ3JwY2Nvbm4iOiJncnBjLm5tLmRldi5zdGF0ZWh1Yi5pbzo0NDMiLCJncnBjaG9zdCI6ImdycGMubm0uZGV2LnN0YXRlaHViLmlvIiwiZ3JwY3BvcnQiOiI1MDA1MSIsImdycGNzc2wiOiJvbiIsImNoZWNraW5pbnRlcnZhbCI6IjE1IiwibmV0d29yayI6ImRhdGFwYXRoIiwia2V5IjoiY3lWWERaNW41M0lBa3B1UCIsImxvY2FscmFuZ2UiOiIiLCJncnBjd2ciOiIiLCJncnBjd2dhZGRyIjoiIiwiZ3JwY3dncG9ydCI6IiIsImdycGN3Z3B1YmtleSI6IiIsImdycGN3Z2VuZHBvaW50IjoiIn0=","uses":191}],"allowmanualsignup":"no","islocal":"no","isdualstack":"no","isipv4":"yes","isipv6":"no","isgrpchub":"no","localrange":"","checkininterval":30,"defaultudpholepunch":"no","defaultextclientdns":"","defaultmtu":1280},
        {"addressrange":"10.201.0.0/16","addressrange6":"fd39:75a7:808f:649d::/64","displayname":"overlap","netid":"overlap","nodeslastmodified":1634663758,"networklastmodified":1634663758,"defaultinterface":"nm-overlap","defaultlistenport":51821,"nodelimit":999999999,"defaultpostup":"","defaultpostdown":"","keyupdatetimestamp":1634663758,"defaultkeepalive":20,"defaultsaveconfig":"no","accesskeys":null,"allowmanualsignup":"no","islocal":"no","isdualstack":"no","isipv4":"yes","isipv6":"no","isgrpchub":"no","localrange":"","checkininterval":30,"defaultudpholepunch":"no","defaultextclientdns":"","defaultmtu":1280}
    ]"#;

// This is what cycle public keys returns on success  - everything is empty :()
// {"addressrange":"","addressrange6":"","netid":"","nodeslastmodified":0,"networklastmodified":0,"defaultinterface":"","nodelimit":0,"defaultpostup":"","defaultpostdown":"","keyupdatetimestamp":0,"defaultkeepalive":0,"defaultsaveconfig":"","accesskeys":null,"allowmanualsignup":"","islocal":"","isdualstack":"","isipv4":"","isipv6":"","isgrpchub":"","localrange":"","defaultudpholepunch":"","defaultextclientdns":"","defaultmtu":0}
#[test]
fn de() {
    let nodes: Vec<Network> = json::from_str(NETWORKS).unwrap();
    assert_eq!(nodes.len(), 2);
}
