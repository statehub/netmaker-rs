//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::net;

use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Node {
    id: String,             // `json:"id,omitempty" bson:"id,omitempty"`
    address: net::Ipv4Addr, // `json:"address" bson:"address" yaml:"address" validate:"omitempty,ipv4"`
    #[serde(with = "empty_string_as_none")]
    address6: Option<net::Ipv6Addr>, // `json:"address6" bson:"address6" yaml:"address6" validate:"omitempty,ipv6"`
    #[serde(with = "empty_string_as_none", rename = "localaddress")]
    local_address: Option<net::IpAddr>, // `json:"localaddress" bson:"localaddress" yaml:"localaddress" validate:"omitempty,ip"`
    name: String, // `json:"name" bson:"name" yaml:"name" validate:"omitempty,max=12,in_charset"`
    #[serde(rename = "listenport")]
    listen_port: u16, // `json:"listenport" bson:"listenport" yaml:"listenport" validate:"omitempty,numeric,min=1024,max=65535"`
    #[serde(rename = "publickey")]
    public_key: String, // `json:"publickey" bson:"publickey" yaml:"publickey" validate:"required,base64"`
    endpoint: net::IpAddr, // `json:"endpoint" bson:"endpoint" yaml:"endpoint" validate:"required,ip"`
    #[serde(rename = "postup")]
    post_up: String, // `json:"postup" bson:"postup" yaml:"postup"`
    #[serde(rename = "postdown")]
    post_down: String, // `json:"postdown" bson:"postdown" yaml:"postdown"`
    #[serde(rename = "allowedips")]
    allowed_ips: Vec<String>, // `json:"allowedips" bson:"allowedips" yaml:"allowedips"`
    #[serde(rename = "persistentkeepalive")]
    persistent_keepalive: u32, // `json:"persistentkeepalive" bson:"persistentkeepalive" yaml:"persistentkeepalive" validate:"omitempty,numeric,max=1000"`
    #[serde(with = "yesno", rename = "saveconfig")]
    save_config: bool, // `json:"saveconfig" bson:"saveconfig" yaml:"saveconfig" validate:"checkyesorno"`
    #[serde(rename = "accesskey")]
    access_key: String, // `json:"accesskey" bson:"accesskey" yaml:"accesskey"`
    interface: String, // `json:"interface" bson:"interface" yaml:"interface"`
    #[serde(rename = "lastmodified")]
    last_modified: i64, // `json:"lastmodified" bson:"lastmodified" yaml:"lastmodified"`
    #[serde(rename = "keyupdatetimestamp")]
    key_update_timestamp: i64, // `json:"keyupdatetimestamp" bson:"keyupdatetimestamp" yaml:"keyupdatetimestamp"`
    #[serde(rename = "expdatetime")]
    expiration_datetime: i64, // `json:"expdatetime" bson:"expdatetime" yaml:"expdatetime"`
    #[serde(rename = "lastpeerupdate")]
    last_peer_update: i64, // `json:"lastpeerupdate" bson:"lastpeerupdate" yaml:"lastpeerupdate"`
    #[serde(rename = "lastcheckin")]
    last_checkin: i64, // `json:"lastcheckin" bson:"lastcheckin" yaml:"lastcheckin"`
    #[serde(rename = "macaddress")]
    mac_address: String, // `json:"macaddress" bson:"macaddress" yaml:"macaddress" validate:"required,mac,macaddress_unique"`
    // checkin interval is depreciated at the network level. Set on server with CHECKIN_INTERVAL
    #[serde(rename = "checkininterval")]
    checkin_interval: i32, // `json:"checkininterval" bson:"checkininterval" yaml:"checkininterval"`
    password: String, // `json:"password" bson:"password" yaml:"password" validate:"required,min=6"`
    network: String,  // `json:"network" bson:"network" yaml:"network" validate:"network_exists"`
    #[serde(rename = "isrelayed")]
    is_relayed: String, // `json:"isrelayed" bson:"isrelayed" yaml:"isrelayed"`
    #[serde(rename = "ispending")]
    is_pending: String, // `json:"ispending" bson:"ispending" yaml:"ispending"`
    #[serde(with = "yesno", rename = "isrelay")]
    is_relay: bool, // `json:"isrelay" bson:"isrelay" yaml:"isrelay" validate:"checkyesorno"`
    #[serde(rename = "isegressgateway")]
    is_egress_gateway: String, // `json:"isegressgateway" bson:"isegressgateway" yaml:"isegressgateway"`
    #[serde(rename = "isingressgateway")]
    is_ingress_gateway: String, // `json:"isingressgateway" bson:"isingressgateway" yaml:"isingressgateway"`
    #[serde(rename = "egressgatewayranges")]
    egress_gateway_ranges: Vec<String>, // `json:"egressgatewayranges" bson:"egressgatewayranges" yaml:"egressgatewayranges"`
    #[serde(rename = "relayaddrs")]
    relay_addrs: Vec<String>, // `json:"relayaddrs" bson:"relayaddrs" yaml:"relayaddrs"`
    #[serde(rename = "ingressgatewayrange")]
    ingress_gateway_range: String, // `json:"ingressgatewayrange" bson:"ingressgatewayrange" yaml:"ingressgatewayrange"`
    #[serde(with = "yesno", rename = "isstatic")]
    is_static: bool, // `json:"isstatic" bson:"isstatic" yaml:"isstatic" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "udpholepunch")]
    udp_hole_punch: bool, // `json:"udpholepunch" bson:"udpholepunch" yaml:"udpholepunch" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "pullchanges")]
    pull_changes: bool, // `json:"pullchanges" bson:"pullchanges" yaml:"pullchanges" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "dnson")]
    dns_on: bool, // `json:"dnson" bson:"dnson" yaml:"dnson" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "isdualstack")]
    is_dual_stack: bool, // `json:"isdualstack" bson:"isdualstack" yaml:"isdualstack" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "isserver")]
    is_server: bool, // `json:"isserver" bson:"isserver" yaml:"isserver" validate:"checkyesorno"`
    action: String, // `json:"action" bson:"action" yaml:"action"`
    #[serde(with = "yesno", rename = "islocal")]
    is_local: bool, // `json:"islocal" bson:"islocal" yaml:"islocal" validate:"checkyesorno"`
    #[serde(rename = "localrange")]
    local_range: String, // `json:"localrange" bson:"localrange" yaml:"localrange"`
    #[serde(with = "yesno")]
    roaming: bool, // `json:"roaming" bson:"roaming" yaml:"roaming" validate:"checkyesorno"`
    #[serde(with = "yesno", rename = "ipforwarding")]
    ip_forwarding: bool, // `json:"ipforwarding" bson:"ipforwarding" yaml:"ipforwarding" validate:"checkyesorno"`
    os: String, // `json:"os" bson:"os" yaml:"os"`
    mtu: u32,   // `json:"mtu" bson:"mtu" yaml:"mtu"`
}

mod empty_string_as_none {
    use std::str;

    use serde::{de, Deserialize, Deserializer, Serialize, Serializer};

    pub(super) fn serialize<T, S>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
    where
        T: Serialize,
        S: Serializer,
    {
        value.serialize(serializer)
    }

    pub(super) fn deserialize<'de, D, T>(deserializer: D) -> Result<Option<T>, D::Error>
    where
        D: Deserializer<'de>,
        T: str::FromStr,
        <T as str::FromStr>::Err: std::error::Error,
    {
        match String::deserialize(deserializer)?.as_str() {
            "" => Ok(None),
            other => T::from_str(other).map(Some).map_err(de::Error::custom),
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_json as json;

    use super::*;

    const NODES: &str = r#"[
        {"id":"06:df:5a:91:82:4c###datapath","address":"10.200.0.2","address6":"","localaddress":"10.0.81.220","name":"220eu-west-2","listenport":51821,"publickey":"/AvauuSKy6q/GiSovTspbB2lFp9ytID5igVvFuSRAjw=","endpoint":"18.135.130.20","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"cyVXDZ5n53IAkpuP","interface":"nm-datapath","lastmodified":1634727453,"keyupdatetimestamp":1634727453,"expdatetime":1934722681,"lastpeerupdate":1634722681,"lastcheckin":1634727452,"macaddress":"06:df:5a:91:82:4c","checkininterval":0,"password":"$2a$05$2tIvy.6g3Z73xswYcJaeL.iq2tpFov4g6QTRXM.OTn14EaBolWmtS","network":"datapath","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"no","udpholepunch":"no","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"no","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280},
        {"id":"06:e0:3f:46:ff:96###datapath","address":"10.200.0.4","address6":"","localaddress":"10.0.120.134","name":"134eu-west-2","listenport":51821,"publickey":"22307yEmDjkES6mbneKolsTeStrwizwjjcYZmOtZvnQ=","endpoint":"18.135.130.20","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"cyVXDZ5n53IAkpuP","interface":"nm-datapath","lastmodified":1634727456,"keyupdatetimestamp":1634727456,"expdatetime":1934662844,"lastpeerupdate":1634662844,"lastcheckin":1634727456,"macaddress":"06:e0:3f:46:ff:96","checkininterval":0,"password":"$2a$05$IdgyLUtKSm.WAQdIc35lp.xL1E2m.wtea22QwM/u8Z9hjLC27ysd6","network":"datapath","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"no","udpholepunch":"no","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"no","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280},
        {"id":"06:f7:34:99:be:20###datapath","address":"10.200.0.9","address6":"","localaddress":"10.0.122.79","name":"79eu-west-2","listenport":51821,"publickey":"xOBCtV99SnlVfn8vOdKYr9ZRPyNmRil74sDptqRPPQA=","endpoint":"18.135.130.20","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"cyVXDZ5n53IAkpuP","interface":"nm-datapath","lastmodified":1634727477,"keyupdatetimestamp":1634727477,"expdatetime":1934663361,"lastpeerupdate":1634663361,"lastcheckin":1634727476,"macaddress":"06:f7:34:99:be:20","checkininterval":0,"password":"$2a$05$Gsd1qgJ1lLQ6ZGbNkOHbiu.tmzG5cwtxKYuFqUIgav1HnxS7IXMYy","network":"datapath","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"no","udpholepunch":"no","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"no","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280},
        {"id":"60:45:bd:7c:d4:50###datapath","address":"10.200.0.3","address6":"","localaddress":"172.22.0.4","name":"netmakerk3s","listenport":51821,"publickey":"MYcPTF/Q1bamhIBEpM2QeoO1KpZXpqu4Sa1hsLXsoBA=","endpoint":"20.96.30.159","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"cyVXDZ5n53IAkpuP","interface":"nm-datapath","lastmodified":1634727469,"keyupdatetimestamp":1634727469,"expdatetime":1934721166,"lastpeerupdate":1634721166,"lastcheckin":1634727469,"macaddress":"60:45:bd:7c:d4:50","checkininterval":0,"password":"$2a$05$sO2QT29zJ/pkgNbfr9D35eKETw4pSoSktea1GfswkE0WAoQmjXGAu","network":"datapath","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"no","udpholepunch":"no","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"no","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280},
        {"id":"02:2c:2b:11:e9:2b###datapath","address":"10.200.0.1","address6":"","localaddress":"","name":"netmaker","listenport":51821,"publickey":"zRTiuEgeA+N45xcQbJ3+MOEVwG/2f82ijRw4cfH/ayo=","endpoint":"54.241.60.241","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"","interface":"nm-datapath","lastmodified":1634727475,"keyupdatetimestamp":1634727475,"expdatetime":1934647969,"lastpeerupdate":1634647969,"lastcheckin":1634727475,"macaddress":"02:2c:2b:11:e9:2b","checkininterval":0,"password":"$2a$05$MdrgLsfgsIwmX1YJRpFx0u22dbU2Ayxi4m8EQMCLt39DYGG3exbY6","network":"datapath","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"yes","udpholepunch":"yes","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"yes","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280},
        {"id":"02:2c:2b:11:e9:2b###overlap","address":"10.201.0.1","address6":"","localaddress":"","name":"netmaker","listenport":51822,"publickey":"RMniypEBfaz/f/zy/FSVW1RZpOM0TJyjvkfg6eMcQh8=","endpoint":"54.241.60.241","postup":"","postdown":"","allowedips":[],"persistentkeepalive":20,"saveconfig":"no","accesskey":"","interface":"nm-overlap","lastmodified":1634727475,"keyupdatetimestamp":1634727475,"expdatetime":1934663758,"lastpeerupdate":1634663758,"lastcheckin":1634727475,"macaddress":"02:2c:2b:11:e9:2b","checkininterval":0,"password":"$2a$05$B5sFUiTf.hWoJ.3rtzrlR.LskFy8nCA2HGqVjQyLY88Rw236GPuf2","network":"overlap","isrelayed":"no","ispending":"no","isrelay":"no","isegressgateway":"no","isingressgateway":"no","egressgatewayranges":[],"relayaddrs":[],"ingressgatewayrange":"","isstatic":"yes","udpholepunch":"no","pullchanges":"no","dnson":"no","isdualstack":"no","isserver":"yes","action":"noop","islocal":"no","localrange":"","roaming":"yes","ipforwarding":"yes","os":"linux","mtu":1280}
        ]"#;
    #[test]
    fn de() {
        let nodes: Vec<Node> = json::from_str(NODES).unwrap();
        assert_eq!(nodes.len(), 6);
    }
}
