//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde::de;
use serde_json as json;

use super::*;

impl ErrorResponse {
    pub fn from_split_response(code: StatusCode, message: String) -> Self {
        json::from_str::<Self>(&message).unwrap_or(Self { code, message })
    }
}

impl From<json::Error> for ErrorResponse {
    fn from(err: json::Error) -> Self {
        let code = reqwest::StatusCode::BAD_REQUEST;
        let message = format!("Synthetic: {}", err);
        Self::from_split_response(code, message)
    }
}

impl From<reqwest::Error> for ErrorResponse {
    fn from(error: reqwest::Error) -> Self {
        let code = error.status().unwrap_or_default();
        let message = error.to_string();
        Self { code, message }
    }
}

impl<T> TryFrom<bytes::Bytes> for SuccessResponse<T>
where
    T: de::DeserializeOwned,
{
    type Error = json::Error;

    fn try_from(bytes: bytes::Bytes) -> Result<Self, Self::Error> {
        json::from_slice(&bytes)
    }
}

impl AccessKey {
    pub(crate) fn new(name: &str, uses: i32) -> Self {
        let name = name.to_string();
        let value = String::new();
        let access_string = String::new();
        Self {
            name,
            value,
            access_string,
            uses,
        }
    }
}
