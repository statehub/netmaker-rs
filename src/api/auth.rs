//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

#[derive(Debug, Serialize, Deserialize)]
pub struct UserAuthParams {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Serialize, Deserialize)]
// #[serde(rename_all = "PascalCase")]
pub struct SuccessfulUserLoginResponse {
    #[serde(rename = "UserName")]
    pub username: String,
    #[serde(rename = "AuthToken")]
    pub authtoken: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthToken {
    #[serde(rename = "IsAdmin")]
    pub is_admin: bool,
    #[serde(rename = "UserName")]
    pub username: String,
    #[serde(rename = "Networks")]
    pub networks: Option<Vec<String>>,
    #[serde(flatten)]
    pub jwt: jwt::RegisteredClaims,
}

#[cfg(test)]
mod tests;
