//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct CreateNetworkDto {
    #[serde(rename = "addressrange")]
    pub address_range: String, // `json:"addressrange" bson:"addressrange" validate:"required,cidr"`
    pub netid: String, // `json:"netid" bson:"netid" validate:"required,min=1,max=12,netid_valid"`
}

#[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct Network {
    #[serde(rename = "addressrange")]
    pub address_range: String, // `json:"addressrange" bson:"addressrange" validate:"required,cidr"`
    #[serde(rename = "addressrange6")]
    pub address_range6: String, // `json:"addressrange6" bson:"addressrange6" validate:"regexp=^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*(\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8]))?$"`
    #[serde(rename = "displayname")]
    pub display_name: String, // `json:"displayname,omitempty" bson:"displayname,omitempty" validate:"omitempty,min=1,max=20,displayname_valid"`
    pub netid: String, // `json:"netid" bson:"netid" validate:"required,min=1,max=12,netid_valid"`
    #[serde(rename = "nodeslastmodified")]
    pub nodes_last_modified: i64, // `json:"nodeslastmodified" bson:"nodeslastmodified"`
    #[serde(rename = "networklastmodified")]
    pub network_last_modified: i64, // `json:"networklastmodified" bson:"networklastmodified"`
    #[serde(rename = "defaultinterface")]
    pub default_interface: String, // `json:"defaultinterface" bson:"defaultinterface" validate:"min=1,max=15"`
    #[serde(rename = "defaultlistenport")]
    pub default_listen_port: i32, // `json:"defaultlistenport,omitempty" bson:"defaultlistenport,omitempty" validate:"omitempty,min=1024,max=65535"`
    #[serde(rename = "nodelimit")]
    pub node_limit: i32, // `json:"nodelimit" bson:"nodelimit"`
    #[serde(rename = "defaultpostup")]
    pub default_postup: String, // `json:"defaultpostup" bson:"defaultpostup"`
    #[serde(rename = "defaultpostdown")]
    pub default_postdown: String, // `json:"defaultpostdown" bson:"defaultpostdown"`
    #[serde(rename = "keyupdatetimestamp")]
    pub key_update_timestamp: i64, // `json:"keyupdatetimestamp" bson:"keyupdatetimestamp"`
    #[serde(rename = "defaultkeepalive")]
    pub default_keepalive: i32, // `json:"defaultkeepalive" bson:"defaultkeepalive" validate:"omitempty,max=1000"`
    #[serde(with = "yesno", rename = "defaultsaveconfig")]
    pub default_save_config: bool, // `json:"defaultsaveconfig" bson:"defaultsaveconfig" validate:"checkyesorno"`
    #[serde(rename = "accesskeys", default)]
    pub access_keys: Option<Vec<AccessKey>>, // `json:"accesskeys" bson:"accesskeys"`
    #[serde(rename = "allowmanualsignup")]
    pub allow_manual_signup: String, // `json:"allowmanualsignup" bson:"allowmanualsignup" validate:"checkyesorno"`
    #[serde(rename = "islocal")]
    pub is_local: String, // `json:"islocal" bson:"islocal" validate:"checkyesorno"`
    #[serde(rename = "isdualstack")]
    pub is_dual_stack: String, // `json:"isdualstack" bson:"isdualstack" validate:"checkyesorno"`
    #[serde(rename = "isipv4")]
    pub is_ipv4: String, // `json:"isipv4" bson:"isipv4" validate:"checkyesorno"`
    #[serde(rename = "isipv6")]
    pub is_ipv6: String, // `json:"isipv6" bson:"isipv6" validate:"checkyesorno"`
    #[serde(rename = "isgrpchub")]
    pub is_grpchub: String, // `json:"isgrpchub" bson:"isgrpchub" validate:"checkyesorno"`
    #[serde(rename = "localrange")]
    pub local_range: String, // `json:"localrange" bson:"localrange" validate:"omitempty,cidr"`
    // checkin interval is depreciated at the network level. Set on server with CHECKIN_INTERVAL
    #[serde(rename = "checkininterval")]
    pub default_checkin_interval: i32, // `json:"checkininterval,omitempty" bson:"checkininterval,omitempty" validate:"omitempty,numeric,min=2,max=100000"`
    #[serde(rename = "defaultudpholepunch")]
    pub default_udp_hole_punch: String, // `json:"defaultudpholepunch" bson:"defaultudpholepunch" validate:"checkyesorno"`
    #[serde(rename = "defaultextclientdns")]
    pub default_ext_clientdns: String, // `json:"defaultextclientdns" bson:"defaultextclientdns"`
    #[serde(rename = "defaultmtu")]
    pub default_mtu: i32, // `json:"defaultmtu" bson:"defaultmtu"`
}

// AccessKey - access key struct
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AccessKey {
    pub name: String, // `json:"name" bson:"name" validate:"omitempty,max=20"`
    #[serde(skip_serializing_if = "String::is_empty")]
    pub value: String, // `json:"value" bson:"value" validate:"omitempty,alphanum,max=16"`
    #[serde(rename = "accessstring", skip_serializing_if = "String::is_empty")]
    pub access_string: String, // `json:"accessstring" bson:"accessstring"`
    pub uses: i32,    // `json:"uses" bson:"uses"`
}

#[cfg(test)]
mod tests;
