use pretty_assertions::assert_eq;

use super::*;

const JWT: &str = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJc0FkbWluIjp0cnVlLCJVc2VyTmFtZSI6ImZyaWRheSIsIk5ldHdvcmtzIjpudWxsLCJleHAiOjE2MzU3MDcyOTl9.b-VNf8YV9ZnDoC5esleUSsLKFiTpJPOUpNhX-ZuzGY8";
#[test]
fn auth() {
    let token: jwt::Token<jwt::Header, AuthToken, _> = jwt::Token::parse_unverified(JWT).unwrap();
    let claims = token.claims();
    assert_eq!(claims.jwt.expiration, Some(1635707299));
}
